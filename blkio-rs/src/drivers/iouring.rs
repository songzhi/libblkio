// SPDX-License-Identifier: LGPL-2.1-or-later

use crate::properties;
use crate::properties::{properties, PropertiesList, Property};
use crate::{
    Blkioq, Completion, CompletionBacklog, Driver, Error, MemoryRegion, Queue, ReqFlags, Request,
    RequestBacklog, RequestTypeArgs, Result, State,
};
use const_cstr::const_cstr;
use io_uring::opcode::{Fallocate, Fsync, Read, Readv, Write, Writev};
use io_uring::types::{Fixed, FsyncFlags};
use libc::{
    c_int, c_void, dev_t, iovec, sigset_t, FALLOC_FL_KEEP_SIZE, FALLOC_FL_PUNCH_HOLE,
    FALLOC_FL_ZERO_RANGE, O_DIRECT, O_RDWR, O_WRONLY, RWF_DSYNC,
};
use nix::errno::Errno;
use nix::fcntl::{fcntl, FcntlArg};
use nix::sys::eventfd::{eventfd, EfdFlags};
use nix::sys::stat::{major, minor};
use nix::sys::statfs::fstatfs;
use nix::unistd::{close, lseek64, sysconf, SysconfVar, Whence};
use std::cmp;
use std::collections::HashMap;
use std::convert::TryInto;
use std::fs::{self, File, OpenOptions};
use std::io::{self, ErrorKind};
use std::iter;
use std::num::{ParseIntError, Wrapping};
use std::os::linux::fs::MetadataExt;
use std::os::unix::fs::{FileTypeExt, OpenOptionsExt};
use std::os::unix::io::{AsRawFd, FromRawFd, RawFd};
use std::path::{Path, PathBuf};
use std::ptr;
use std::ptr::null_mut;
use std::str::FromStr;
use std::time::Duration;

// The <linux/io_uring.h> constant is missing from the io_uring crate
const IORING_MAX_ENTRIES: i32 = 32768;

// Hardware queue depth 64-128 is common so use that as the default
const NUM_DESCS_DEFAULT: i32 = 128;

// The io_uring crate exposes a low-level io_uring_enter(2) interface via IoUring.enter() but the
// flag argument constants are private in io_uring::sys. Redefine the value from <linux/io_uring.h>
// here for now.
const IORING_ENTER_GETEVENTS: u32 = 1;

/// Reads a sysfs attribute as an integer
fn sysfs_attr_read<P, T>(path: P) -> io::Result<T>
where
    P: AsRef<Path>,
    T: FromStr<Err = ParseIntError>,
{
    let contents = fs::read(path)?;
    String::from_utf8_lossy(&contents)
        .trim()
        .parse()
        .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))
}

/// Linux block device queue limits
#[derive(Copy, Clone, Debug)]
struct LinuxBlockLimits {
    logical_block_size: u32,
    physical_block_size: u32,
    optimal_io_size: u32,
    write_zeroes_max_bytes: u64,
    discard_alignment: u32,
    discard_alignment_offset: u32,
    supports_fua_natively: bool,
}

impl LinuxBlockLimits {
    fn from_device_number(device_number: dev_t) -> io::Result<LinuxBlockLimits> {
        // build sysfs paths

        let dev_dir = PathBuf::from(format!(
            "/sys/dev/block/{}:{}",
            major(device_number),
            minor(device_number)
        ));

        let is_partition = match dev_dir.join("partition").metadata() {
            Ok(_) => true,
            Err(e) if e.kind() == ErrorKind::NotFound => false,
            Err(e) => return Err(e),
        };

        let queue_dir = dev_dir.join(if is_partition { "../queue" } else { "queue" });

        let dev_attr = |name| sysfs_attr_read(dev_dir.join(name));
        let queue_attr_u32 = |name| -> io::Result<u32> { sysfs_attr_read(queue_dir.join(name)) };
        let queue_attr_u64 = |name| -> io::Result<u64> { sysfs_attr_read(queue_dir.join(name)) };

        // retrieve limits

        // As of Linux 5.17, several block drivers report the discard_alignment queue limit
        // incorrectly, setting it to the discard_granularity instead of 0, which we fix here.
        let discard_alignment = queue_attr_u32("discard_granularity")?;
        let discard_alignment_offset = {
            let value = dev_attr("discard_alignment")?;
            if value == discard_alignment {
                0
            } else {
                value
            }
        };

        let supports_fua_natively = queue_attr_u32("fua")? != 0;

        // NOTE: When adding more fields to LinuxBlockLimits, either make sure the queried sysfs
        // files exist in all kernel versions where io_uring may be available, or fall back to a
        // default value if they are missing.

        Ok(LinuxBlockLimits {
            logical_block_size: queue_attr_u32("logical_block_size")?,
            physical_block_size: queue_attr_u32("physical_block_size")?,
            optimal_io_size: queue_attr_u32("optimal_io_size")?,
            write_zeroes_max_bytes: queue_attr_u64("write_zeroes_max_bytes")?,
            discard_alignment,
            discard_alignment_offset,
            supports_fua_natively,
        })
    }
}

/// Information about the block device or regular file that is the target of an [`IoUring`] instance
#[derive(Copy, Clone, Debug)]
struct TargetInfo {
    is_block_device: bool,
    direct: bool,
    read_only: bool,
    request_alignment: i32,
    optimal_io_alignment: i32,
    optimal_io_size: i32,
    supports_write_zeroes_without_fallback: bool,
    discard_alignment: i32,
    discard_alignment_offset: i32,
    supports_fua_natively: bool,
}

impl TargetInfo {
    fn from_file(file: &File) -> io::Result<TargetInfo> {
        let meta = file.metadata()?;

        let file_status_flags = fcntl(file.as_raw_fd(), FcntlArg::F_GETFL)?;

        let direct = file_status_flags & O_DIRECT != 0;
        let read_only = file_status_flags & (O_WRONLY | O_RDWR) == 0;

        if meta.file_type().is_block_device() {
            let limits = LinuxBlockLimits::from_device_number(meta.st_rdev())?;

            let request_alignment = if direct {
                limits.logical_block_size as i32
            } else {
                1
            };

            Ok(TargetInfo {
                is_block_device: true,
                direct,
                read_only,
                request_alignment,
                optimal_io_alignment: limits.physical_block_size as i32,
                optimal_io_size: limits.optimal_io_size as i32,
                supports_write_zeroes_without_fallback: limits.write_zeroes_max_bytes > 0,
                discard_alignment: limits.discard_alignment as i32,
                discard_alignment_offset: limits.discard_alignment_offset as i32,
                supports_fua_natively: limits.supports_fua_natively,
            })
        } else {
            // This can fail if the file system is not backed by a real block device.
            let block_limits = LinuxBlockLimits::from_device_number(meta.st_dev()).ok();

            let request_alignment = if direct {
                // Fall back to the page size, which should always be enough alignment.
                match block_limits {
                    Some(limits) => limits.logical_block_size as i32,
                    None => sysconf(SysconfVar::PAGE_SIZE)?.unwrap() as i32,
                }
            } else {
                1
            };

            // Conservatively fall back to reporting no native FUA support.
            let supports_fua_natively = match block_limits {
                Some(limits) => limits.supports_fua_natively,
                None => false,
            };

            let file_system_block_size = fstatfs(file)?.block_size() as i32;

            Ok(TargetInfo {
                is_block_device: false,
                direct,
                read_only,
                request_alignment,
                optimal_io_alignment: cmp::max(file_system_block_size, request_alignment),
                optimal_io_size: 0,
                supports_write_zeroes_without_fallback: true,
                // (Correct) file systems don't place alignment restrictions on fallocate(), but
                // property "discard-alignment" must be a multiple of property "request-alignment".
                discard_alignment: request_alignment,
                discard_alignment_offset: 0,
                supports_fua_natively,
            })
        }
    }
}
struct ReqContext {
    expected_ret: usize,
    user_data: usize,
}

struct Requests {
    next_key: Wrapping<u64>,
    in_flight: HashMap<u64, ReqContext>,
}
impl Requests {
    fn new(capacity: u32) -> Self {
        Requests {
            next_key: Wrapping(0),
            in_flight: HashMap::with_capacity(capacity as usize),
        }
    }
    fn insert(&mut self, req: ReqContext) -> u64 {
        let key = self.next_key.0;

        // it's safe to wrap around, since it's impossible to have all these requests in flight,
        // for the same reason we don't check if the 'key' already exists in the HashMap.
        // We need to use 'Wrapping(1)' to support older rust compilers
        self.next_key += Wrapping(1);
        self.in_flight.insert(key, req);
        key
    }

    fn remove(&mut self, key: u64) -> Option<ReqContext> {
        self.in_flight.remove(&key)
    }
}

struct IoUringQueue {
    target_info: TargetInfo,
    ring: io_uring::IoUring,
    iovecs: Box<[iovec]>,
    iovecs_used: usize,
    num_submitted: usize,
    supports_read: bool,
    supports_write: bool,
    supports_fallocate: bool,
    eventfd: RawFd,
    requests: Requests,
}

impl IoUringQueue {
    pub fn new(num_descs: u32, fd: RawFd, target_info: &TargetInfo) -> Result<Self> {
        let ring = io_uring::IoUring::new(num_descs)
            .map_err(|e| Error::from_io_error(e, Errno::ENOMEM))?;

        // io_uring functionality probing was introduced simultaneously with
        // support for (non-vectored) read, write, and fallocate in kernel
        // version 5.6. If the probe fails, we assume that the kernel doesn't
        // support any of these ops. Otherwise, we use it to check for their
        // availability, just in case the kernel in use was patched to include
        // probing but not those ops.

        let mut supports_read = false;
        let mut supports_write = false;
        let mut supports_fallocate = false;

        let mut probe = io_uring::Probe::new();

        if ring.submitter().register_probe(&mut probe).is_ok() {
            supports_read = probe.is_supported(Read::CODE);
            supports_write = probe.is_supported(Write::CODE);
            supports_fallocate = probe.is_supported(Fallocate::CODE);
        }

        ring.submitter()
            .register_files(&[fd])
            .map_err(|e| Error::from_io_error(e, Errno::ENOTSUP))?;

        let eventfd = eventfd(0, EfdFlags::EFD_CLOEXEC | EfdFlags::EFD_NONBLOCK)?;

        let zero_iovec = iovec {
            iov_base: null_mut(),
            iov_len: 0,
        };

        // create IoUringQueue here so eventfd is closed on error
        let queue = IoUringQueue {
            target_info: *target_info,
            ring,
            iovecs: vec![zero_iovec; num_descs.try_into().unwrap()].into_boxed_slice(),
            iovecs_used: 0,
            num_submitted: 0,
            supports_read,
            supports_write,
            supports_fallocate,
            eventfd,
            requests: Requests::new(num_descs),
        };

        queue
            .ring
            .submitter()
            .register_eventfd(eventfd)
            .map_err(|e| Error::from_io_error(e, Errno::ENOTSUP))?;

        Ok(queue)
    }

    fn try_readv_for_read(
        &mut self,
        completion_backlog: &mut CompletionBacklog,
        start: u64,
        buf: *mut u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) -> bool {
        if self.iovecs_used == self.iovecs.len() {
            return false;
        }

        self.iovecs[self.iovecs_used] = iovec {
            iov_base: buf as *mut c_void,
            iov_len: len,
        };

        let readv_req = Request {
            args: RequestTypeArgs::Readv {
                start,
                iovec: &self.iovecs[self.iovecs_used] as *const iovec,
                iovcnt: 1,
            },
            user_data,
            flags,
        };

        if !self.try_enqueue(completion_backlog, &readv_req) {
            return false;
        }

        self.iovecs_used += 1;
        true
    }

    fn try_writev_for_write(
        &mut self,
        completion_backlog: &mut CompletionBacklog,
        start: u64,
        buf: *const u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) -> bool {
        if self.iovecs_used == self.iovecs.len() {
            return false;
        }

        self.iovecs[self.iovecs_used] = iovec {
            iov_base: buf as *mut c_void,
            iov_len: len,
        };

        let writev_req = Request {
            args: RequestTypeArgs::Writev {
                start,
                iovec: &self.iovecs[self.iovecs_used] as *const iovec,
                iovcnt: 1,
            },
            user_data,
            flags,
        };

        if !self.try_enqueue(completion_backlog, &writev_req) {
            return false;
        }

        self.iovecs_used += 1;
        true
    }
}

impl Drop for IoUringQueue {
    fn drop(&mut self) {
        let _ = close(self.eventfd);
    }
}

fn iovec_total_bytes(iovec: *const libc::iovec, iovcnt: u32) -> usize {
    let io_vectors = unsafe { std::slice::from_raw_parts(iovec, iovcnt as usize) };
    io_vectors.iter().map(|iov| iov.iov_len).sum()
}

impl Queue for IoUringQueue {
    fn get_completion_fd(&self) -> RawFd {
        self.eventfd
    }

    fn set_completion_fd_enabled(&mut self, _enabled: bool) {
        // TODO: Set/unset IORING_CQ_EVENTFD_DISABLED. The io-uring crate
        // doesn't support this yet.
    }

    fn try_enqueue(&mut self, completion_backlog: &mut CompletionBacklog, req: &Request) -> bool {
        let request_id;

        let entry = match *req {
            Request {
                args: RequestTypeArgs::Read { start, buf, len },
                user_data,
                flags,
            } => {
                if len > u32::MAX as usize {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::EINVAL,
                        const_cstr!("len must fit in an unsigned 32-bit integer"),
                    ));
                    return true;
                }

                if !self.supports_read {
                    return self.try_readv_for_read(
                        completion_backlog,
                        start,
                        buf,
                        len,
                        user_data,
                        flags,
                    );
                }

                // We should insert the request after checking for supports_read to avoid
                // duplicated entries
                request_id = self.requests.insert(ReqContext {
                    expected_ret: len,
                    user_data,
                });

                Read::new(Fixed(0), buf, len as u32)
                    .offset(start as i64)
                    .build()
                    .user_data(request_id)
            }
            Request {
                args: RequestTypeArgs::Write { start, buf, len },
                user_data,
                flags,
            } => {
                if len > u32::MAX as usize {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::EINVAL,
                        const_cstr!("len must fit in an unsigned 32-bit integer"),
                    ));
                    return true;
                }

                if !self.supports_write {
                    return self.try_writev_for_write(
                        completion_backlog,
                        start,
                        buf,
                        len,
                        user_data,
                        flags,
                    );
                }

                let rw_flags = if flags.contains(ReqFlags::FUA) {
                    RWF_DSYNC
                } else {
                    0
                };

                // We should insert the request after checking for supports_write to avoid
                // duplicated entries
                request_id = self.requests.insert(ReqContext {
                    expected_ret: len,
                    user_data,
                });

                Write::new(Fixed(0), buf, len as u32)
                    .offset(start as i64)
                    .rw_flags(rw_flags)
                    .build()
                    .user_data(request_id)
            }
            Request {
                args:
                    RequestTypeArgs::Readv {
                        start,
                        iovec,
                        iovcnt,
                    },
                user_data,
                ..
            } => {
                let len = iovec_total_bytes(iovec, iovcnt);
                request_id = self.requests.insert(ReqContext {
                    expected_ret: len,
                    user_data,
                });
                Readv::new(Fixed(0), iovec, iovcnt)
                    .offset(start as i64)
                    .build()
                    .user_data(request_id)
            }
            Request {
                args:
                    RequestTypeArgs::Writev {
                        start,
                        iovec,
                        iovcnt,
                    },
                user_data,
                flags,
            } => {
                let rw_flags = if flags.contains(ReqFlags::FUA) {
                    RWF_DSYNC
                } else {
                    0
                };
                let len = iovec_total_bytes(iovec, iovcnt);
                request_id = self.requests.insert(ReqContext {
                    expected_ret: len,
                    user_data,
                });
                Writev::new(Fixed(0), iovec, iovcnt)
                    .offset(start as i64)
                    .rw_flags(rw_flags)
                    .build()
                    .user_data(request_id)
            }
            Request {
                args: RequestTypeArgs::Flush,
                user_data,
                ..
            } => {
                request_id = self.requests.insert(ReqContext {
                    expected_ret: 0,
                    user_data,
                });
                Fsync::new(Fixed(0))
                    .flags(FsyncFlags::DATASYNC)
                    .build()
                    .user_data(request_id)
            }
            Request {
                args: RequestTypeArgs::WriteZeroes { start, len },
                user_data,
                flags,
            } => {
                if !self.supports_fallocate {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::ENOTSUP,
                        const_cstr!("the kernel does not support IORING_OP_FALLOCATE"),
                    ));
                    return true;
                }

                #[allow(clippy::collapsible_else_if)]
                let mode = if self.target_info.is_block_device {
                    if self.target_info.supports_write_zeroes_without_fallback {
                        if flags.contains(ReqFlags::NO_UNMAP) {
                            // This will make the kernel call `blkdev_issue_zeroout(...,
                            // BLKDEV_ZERO_NOUNMAP)`, which in this case will simply submit a
                            // REQ_OP_WRITE_ZEROES request with flag REQ_NOUNMAP.
                            FALLOC_FL_ZERO_RANGE | FALLOC_FL_KEEP_SIZE
                        } else {
                            // This will make the kernel call `blkdev_issue_zeroout(...,
                            // BLKDEV_ZERO_NOFALLBACK)`, which in this case will simply submit a
                            // REQ_OP_WRITE_ZEROES request without flag REQ_NOUNMAP.
                            FALLOC_FL_PUNCH_HOLE | FALLOC_FL_KEEP_SIZE
                        }
                    } else {
                        if flags.contains(ReqFlags::NO_FALLBACK) {
                            completion_backlog.push(Completion::for_failed_req(
                                req,
                                Errno::ENOTSUP,
                                const_cstr!("the block device does not support write zeroes with BLKIO_REQ_NO_FALLBACK"),
                            ));
                            return true;
                        }

                        // This will make the kernel call `blkdev_issue_zeroout(...,
                        // BLKDEV_ZERO_NOUNMAP)`, which in this case will emulate a write zeroes request
                        // using regular writes.
                        FALLOC_FL_ZERO_RANGE | FALLOC_FL_KEEP_SIZE
                    }
                } else {
                    if flags.contains(ReqFlags::NO_UNMAP) {
                        FALLOC_FL_ZERO_RANGE
                    } else {
                        // This does not update the file size when requests extend past EOF, but our docs
                        // specify that io_uring write zeroes requests on regular files may or may not
                        // update the file size, so this behavior is correct.
                        FALLOC_FL_PUNCH_HOLE | FALLOC_FL_KEEP_SIZE
                    }
                };

                request_id = self.requests.insert(ReqContext {
                    expected_ret: 0,
                    user_data,
                });
                Fallocate::new(Fixed(0), len as i64)
                    .offset(start as i64)
                    .mode(mode)
                    .build()
                    .user_data(request_id)
            }
            Request {
                args: RequestTypeArgs::Discard { start, len },
                user_data,
                ..
            } => {
                if !self.supports_fallocate {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::ENOTSUP,
                        const_cstr!("the kernel does not support IORING_OP_FALLOCATE"),
                    ));
                    return true;
                }

                const FALLOC_FL_NO_HIDE_STALE: c_int = 0x04;
                request_id = self.requests.insert(ReqContext {
                    expected_ret: 0,
                    user_data,
                });
                Fallocate::new(Fixed(0), len as i64)
                    .offset(start as i64)
                    .mode(FALLOC_FL_PUNCH_HOLE | FALLOC_FL_NO_HIDE_STALE | FALLOC_FL_KEEP_SIZE)
                    .build()
                    .user_data(request_id)
            }
        };

        let result = unsafe { self.ring.submission().push(&entry) };
        if result.is_err() {
            let _ = self.requests.remove(request_id);
        }
        result.is_ok()
    }

    fn do_io(
        &mut self,
        request_backlog: &mut RequestBacklog,
        completion_backlog: &mut CompletionBacklog,
        completions: &mut [std::mem::MaybeUninit<Completion>],
        min_completions: usize,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<usize> {
        // TODO implement timeout with Linux 5.11 IORING_ENTER_EXT_ARG in the future
        if timeout.is_some() {
            return Err(Error::new(Errno::ENOTSUP, "timeout not yet implemented"));
        }

        // filled_completions tracks how many elements of completions[] have been filled in
        let mut filled_completions = completion_backlog.fill_completions(completions);

        // Fill completions[] from the cq ring and return the count
        fn drain_cqueue(
            ring: &mut io_uring::IoUring,
            completions: &mut [std::mem::MaybeUninit<Completion>],
            requests: &mut Requests,
        ) -> usize {
            let mut cqueue = ring.completion();
            let mut i = 0;
            while i < completions.len() {
                if let Some(cqe) = cqueue.next() {
                    let (expected_ret, user_data) = requests
                        .remove(cqe.user_data())
                        .map(|req| (req.expected_ret, req.user_data))
                        .expect("All in-flight requests are tracked");

                    let ret = if cqe.result() < 0 {
                        cqe.result()
                    } else if expected_ret == cqe.result() as usize {
                        0
                    } else {
                        -libc::EIO
                    };

                    let c = Completion {
                        user_data,
                        ret,
                        error_msg: ptr::null(),
                    };
                    unsafe { completions[i].as_mut_ptr().write(c) };
                    i += 1;
                } else {
                    break;
                }
            }
            i
        }

        let n = drain_cqueue(
            &mut self.ring,
            &mut completions[filled_completions..],
            &mut self.requests,
        );
        filled_completions += n;
        self.num_submitted -= n;

        let mut to_submit = self.ring.submission().len();

        if min_completions
            > filled_completions + self.num_submitted + to_submit + request_backlog.len()
        {
            completion_backlog.unfill_completions(completions, filled_completions);
            return Err(Error::new(
                Errno::EINVAL,
                "min_completions is larger than total outstanding requests",
            ));
        }

        while filled_completions < min_completions || to_submit > 0 {
            let min_complete = if filled_completions < min_completions {
                let in_flight = self.num_submitted + to_submit;

                // Clamp to number of in-flight requests to avoid hangs when the user provides a
                // min_completions number that is too large.
                std::cmp::min(min_completions - filled_completions, in_flight)
            } else {
                0
            };

            let flags = if min_complete > 0 {
                IORING_ENTER_GETEVENTS
            } else {
                0
            };

            let num_submitted = match unsafe {
                self.ring
                    .submitter()
                    .enter(to_submit as u32, min_complete as u32, flags, sig)
                    .map_err(|e| Error::from_io_error(e, Errno::EINVAL))
            } {
                Ok(n) => n,
                // TODO document EAGAIN/EBUSY or try again with to_submit=0 just to reap
                // completions and wait for enough resources to submit again?
                Err(err) => {
                    completion_backlog.unfill_completions(completions, filled_completions);
                    return Err(err);
                }
            };
            self.num_submitted += num_submitted;

            let n = drain_cqueue(
                &mut self.ring,
                &mut completions[filled_completions..],
                &mut self.requests,
            );
            filled_completions += n;
            self.num_submitted -= n;

            // Clearing iovecs_used assumes all requests were submitted
            assert!(num_submitted == to_submit);
            self.iovecs_used = 0;

            if num_submitted > 0 {
                request_backlog.process(self, completion_backlog);
            }

            to_submit = self.ring.submission().len();
        }

        Ok(filled_completions)
    }
}

properties! {
    IOURING_PROPS: PropertyState for IoUring.props {
        fn buf_alignment: i32,
        fn capacity: u64,
        mut direct: bool,
        fn discard_alignment: i32,
        fn discard_alignment_offset: i32,
        driver: str,
        mut fd: i32,
        max_descs: i32,
        fn max_discard_len: u64,
        max_queues: i32,
        max_mem_regions: u64,
        fn max_segments: i32,
        fn max_transfer: i32,
        fn max_write_zeroes_len: u64,
        fn mem_region_alignment: u64,
        needs_mem_regions: bool,
        needs_mem_region_fd: bool,
        mut num_descs: i32,
        mut num_queues: i32,
        fn optimal_io_alignment: i32,
        fn optimal_io_size: i32,
        fn optimal_buf_alignment: i32,
        mut path: str,
        mut read_only: bool,
        fn request_alignment: i32,
        supports_fua_natively: bool
    }
}

pub struct IoUring {
    props: PropertyState,
    file: Option<File>,
    target_info: Option<TargetInfo>,
    queues: Vec<Blkioq>,
    state: State,
}

impl IoUring {
    pub fn new() -> Self {
        IoUring {
            props: PropertyState {
                direct: false,
                driver: "io_uring".to_string(),
                fd: -1,
                max_descs: IORING_MAX_ENTRIES,
                max_queues: i32::MAX,
                max_mem_regions: u64::MAX,
                needs_mem_regions: false,
                needs_mem_region_fd: false,
                num_descs: NUM_DESCS_DEFAULT,
                num_queues: 1,
                path: String::new(),
                read_only: false,
                supports_fua_natively: false,
            },
            file: None,
            target_info: None,
            queues: Vec::new(),
            state: State::Created,
        }
    }

    fn cant_set_while_connected(&self) -> Result<()> {
        if self.state >= State::Connected {
            Err(properties::error_cant_set_while_connected())
        } else {
            Ok(())
        }
    }

    fn cant_set_while_started(&self) -> Result<()> {
        if self.state >= State::Started {
            Err(properties::error_cant_set_while_started())
        } else {
            Ok(())
        }
    }

    fn must_be_connected(&self) -> Result<()> {
        if self.state >= State::Connected {
            Ok(())
        } else {
            Err(properties::error_must_be_connected())
        }
    }

    fn must_be_started(&self) -> Result<()> {
        if self.state >= State::Started {
            Ok(())
        } else {
            Err(Error::new(Errno::EBUSY, "Device must be started"))
        }
    }

    fn get_capacity(&self) -> Result<u64> {
        self.must_be_connected()?;
        Ok(lseek64(self.props.fd, 0, Whence::SeekEnd)? as u64)
    }

    fn set_direct(&mut self, value: bool) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.direct = value;
        Ok(())
    }

    fn set_fd(&mut self, value: i32) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.fd = value;
        Ok(())
    }

    // Open the file into self.fd
    fn open_file(&mut self) -> Result<()> {
        if !self.props.path.is_empty() {
            if self.props.fd != -1 {
                return Err(Error::new(
                    Errno::EINVAL,
                    "path and fd cannot be set at the same time",
                ));
            }

            let open_flags = if self.props.direct { O_DIRECT } else { 0 };

            let file = OpenOptions::new()
                .custom_flags(open_flags)
                .read(true)
                .write(!self.props.read_only)
                .open(self.props.path.as_str())
                .map_err(|e| Error::from_io_error(e, Errno::EINVAL))?;

            self.props.fd = file.as_raw_fd();
            self.assign_file(file)
        } else if self.props.fd != -1 {
            let file = unsafe { File::from_raw_fd(self.props.fd) };
            self.assign_file(file)
        } else {
            Err(Error::new(Errno::EINVAL, "One of path and fd must be set"))
        }
    }

    fn assign_file(&mut self, file: File) -> Result<()> {
        let file_type = file
            .metadata()
            .map_err(|e| Error::from_io_error(e, Errno::EINVAL))?
            .file_type();

        if !file_type.is_block_device() && !file_type.is_file() {
            return Err(Error::new(
                Errno::EINVAL,
                "The file must be a block device or a regular file",
            ));
        }

        let target_info =
            TargetInfo::from_file(&file).map_err(|e| Error::from_io_error(e, Errno::EINVAL))?;

        // Set the 'direct' and 'read-only' properties to match the file's actual status flags, in
        // case the user specified it through the 'fd' property.
        self.props.direct = target_info.direct;
        self.props.read_only = target_info.read_only;

        self.props.supports_fua_natively = target_info.supports_fua_natively;

        self.file = Some(file);
        self.target_info = Some(target_info);

        Ok(())
    }

    fn get_max_segments(&self) -> Result<i32> {
        self.must_be_connected()?;

        // Userspace can submit up to IOV_MAX and the Linux block layer will split requests as
        // needed.
        Ok(sysconf(SysconfVar::IOV_MAX)?.unwrap() as i32)
    }

    fn get_max_transfer(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(0) // unlimited, Linux block layer will split requests if necessary
    }

    fn get_max_write_zeroes_len(&self) -> Result<u64> {
        self.must_be_connected()?;
        Ok(0) // unlimited, Linux block layer will split requests if necessary
    }

    fn get_max_discard_len(&self) -> Result<u64> {
        self.must_be_connected()?;
        Ok(0) // unlimited, Linux block layer will split requests if necessary
    }

    fn get_mem_region_alignment(&self) -> Result<u64> {
        // no alignment restrictions but must be multiple of buf-alignment
        Ok(self.get_buf_alignment()? as u64)
    }

    fn get_buf_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;
        self.get_request_alignment()
    }

    fn set_num_descs(&mut self, value: i32) -> Result<()> {
        self.must_be_connected()?;
        self.cant_set_while_started()?;

        // TODO check power of two?
        if value <= 0 {
            return Err(Error::new(
                Errno::EINVAL,
                "num_descs must be greater than 0",
            ));
        }
        if value > IORING_MAX_ENTRIES {
            return Err(Error::new(
                Errno::EINVAL,
                format!("num_descs must be smaller than {}", IORING_MAX_ENTRIES),
            ));
        }

        self.props.num_descs = value;
        Ok(())
    }

    fn set_num_queues(&mut self, value: i32) -> Result<()> {
        self.must_be_connected()?;
        self.cant_set_while_started()?;

        if value <= 0 {
            return Err(Error::new(
                Errno::EINVAL,
                "num_queues must be greater than 0",
            ));
        }

        self.props.num_queues = value;
        Ok(())
    }

    fn get_optimal_io_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().optimal_io_alignment)
    }

    fn get_optimal_io_size(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().optimal_io_size)
    }

    fn get_optimal_buf_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(sysconf(SysconfVar::PAGE_SIZE)?.unwrap() as i32)
    }

    fn set_path(&mut self, value: &str) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.path = value.to_string();
        Ok(())
    }

    fn set_read_only(&mut self, value: bool) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.read_only = value;
        Ok(())
    }

    fn get_request_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().request_alignment)
    }

    fn get_discard_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().discard_alignment)
    }

    fn get_discard_alignment_offset(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().discard_alignment_offset)
    }
}

impl Driver for IoUring {
    fn state(&self) -> State {
        self.state
    }

    fn connect(&mut self) -> Result<()> {
        self.cant_set_while_started()?;

        if self.state == State::Connected {
            return Ok(());
        }

        self.open_file()?;
        self.state = State::Connected;
        Ok(())
    }

    fn start(&mut self) -> Result<()> {
        self.must_be_connected()?;

        if self.state == State::Started {
            return Ok(());
        }

        let target_info = self.target_info.as_ref().unwrap();

        let create_queue = || {
            let q = IoUringQueue::new(self.props.num_descs as u32, self.props.fd, target_info)?;
            Ok(Blkioq::new(Box::new(q)))
        };

        let queues: Result<Vec<_>> = iter::repeat_with(create_queue)
            .take(self.props.num_queues as usize)
            .collect();

        self.queues = queues?;
        self.state = State::Started;

        Ok(())
    }

    // IORING_REGISTER_BUFFERS could be used in the future to improve performance. Ignore
    // memory regions for now.
    fn add_mem_region(&mut self, _region: &MemoryRegion) -> Result<()> {
        self.must_be_started()
    }

    fn del_mem_region(&mut self, _region: &MemoryRegion) -> Result<()> {
        self.must_be_started()
    }

    fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq> {
        self.queues
            .get_mut(index)
            .ok_or_else(|| Error::new(Errno::EINVAL, "invalid queue index"))
    }
}
