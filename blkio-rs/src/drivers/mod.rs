// SPDX-License-Identifier: LGPL-2.1-or-later

#[cfg(feature = "driver-io-uring")]
pub mod iouring;

#[cfg(any(feature = "driver-vhost-user", feature = "driver-vhost-vdpa"))]
pub mod virtio_blk;
