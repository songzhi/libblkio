// SPDX-License-Identifier: LGPL-2.1-or-later

use crate::properties;
use crate::properties::{properties, PropertiesList, Property};
use crate::{
    Blkioq, Completion, CompletionBacklog, Driver, Error, MemoryRegion, ReqFlags, Request,
    RequestBacklog, RequestTypeArgs, Result, State,
};
use const_cstr::const_cstr;
use libc::sigset_t;
use nix::errno::Errno;
use nix::fcntl::{fcntl, FcntlArg, OFlag};
use std::cell::{RefCell, RefMut};
use std::os::unix::io::{AsRawFd, RawFd};
use std::ptr;
use std::rc::Rc;
use std::time::Duration;
use virtio_driver::{
    VirtioBlkFeatureFlags, VirtioBlkQueue, VirtioBlkTransport, VirtioFeatureFlags,
};
use vmm_sys_util::eventfd::EventFd;

#[cfg(feature = "driver-vhost-user")]
pub const VHOST_USER_DRIVER: &str = "vhost_user";
#[cfg(feature = "driver-vhost-vdpa")]
pub const VHOST_VDPA_DRIVER: &str = "vhost_vdpa";

// This is the maximum as defined in the virtio spec
const MAX_QUEUE_SIZE: i32 = 32768;
const DEFAULT_QUEUE_SIZE: i32 = 256;

struct ReqContext {
    needs_flush: bool,
    user_data: usize,
}

fn fail_req_due_to_read_only(completion_backlog: &mut CompletionBacklog, req: &Request) {
    completion_backlog.push(Completion::for_failed_req(
        req,
        Errno::EBADF,
        const_cstr!("device is read-only"),
    ));
}

struct Queue<'a> {
    features: VirtioBlkFeatureFlags,
    vq: VirtioBlkQueue<'a, ReqContext>,
    submission_fd: Rc<EventFd>,
    completion_fd: Rc<EventFd>,
    read_only: bool,
}

impl<'a> Queue<'a> {
    pub fn new(
        features: VirtioBlkFeatureFlags,
        vq: VirtioBlkQueue<'a, ReqContext>,
        submission_fd: Rc<EventFd>,
        completion_fd: Rc<EventFd>,
        read_only: bool,
    ) -> Self {
        Queue {
            features,
            vq,
            submission_fd,
            completion_fd,
            read_only,
        }
    }
}

impl crate::Queue for Queue<'_> {
    fn get_completion_fd(&self) -> RawFd {
        self.completion_fd.as_raw_fd()
    }

    fn set_completion_fd_enabled(&mut self, _enabled: bool) {
        // TODO: The transport could possibly disable the completion fd
    }

    fn try_enqueue(&mut self, completion_backlog: &mut CompletionBacklog, req: &Request) -> bool {
        let context = ReqContext {
            user_data: req.user_data,
            needs_flush: req.flags.contains(ReqFlags::FUA)
                && self.features.contains(VirtioBlkFeatureFlags::FLUSH),
        };

        let result = match req.args {
            RequestTypeArgs::Read { start, buf, len } => {
                if virtio_driver::validate_lba(start).is_err() {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::EINVAL,
                        const_cstr!("invalid start offset"),
                    ));
                    return true;
                }

                unsafe { self.vq.read_raw(start, buf, len, context) }
            }
            RequestTypeArgs::Write { start, buf, len } => {
                if virtio_driver::validate_lba(start).is_err() {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::EINVAL,
                        const_cstr!("invalid start offset"),
                    ));
                    return true;
                }

                if self.read_only {
                    fail_req_due_to_read_only(completion_backlog, req);
                    return true;
                }

                unsafe { self.vq.write_raw(start, buf, len, context) }
            }
            RequestTypeArgs::Readv {
                start,
                iovec,
                iovcnt,
            } => {
                if virtio_driver::validate_lba(start).is_err() {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::EINVAL,
                        const_cstr!("invalid start offset"),
                    ));
                    return true;
                }

                unsafe { self.vq.readv(start, iovec, iovcnt as usize, context) }
            }
            RequestTypeArgs::Writev {
                start,
                iovec,
                iovcnt,
            } => {
                if virtio_driver::validate_lba(start).is_err() {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::EINVAL,
                        const_cstr!("invalid start offset"),
                    ));
                    return true;
                }

                if self.read_only {
                    fail_req_due_to_read_only(completion_backlog, req);
                    return true;
                }

                unsafe { self.vq.writev(start, iovec, iovcnt as usize, context) }
            }
            RequestTypeArgs::WriteZeroes { start, len } => {
                if !self.features.contains(VirtioBlkFeatureFlags::WRITE_ZEROES) {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::ENOTSUP,
                        const_cstr!("write zeroes not supported"),
                    ));
                    return true;
                }

                if virtio_driver::validate_lba(start).is_err()
                    || virtio_driver::validate_lba(start + len).is_err()
                {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::EINVAL,
                        const_cstr!("invalid len or start offset"),
                    ));
                    return true;
                }

                if self.read_only {
                    fail_req_due_to_read_only(completion_backlog, req);
                    return true;
                }

                let unmap = !req.flags.contains(ReqFlags::NO_UNMAP);

                self.vq.write_zeroes(start, len, unmap, context)
            }
            RequestTypeArgs::Discard { start, len } => {
                if !self.features.contains(VirtioBlkFeatureFlags::DISCARD) {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::ENOTSUP,
                        const_cstr!("discard not supported"),
                    ));
                    return true;
                }

                if virtio_driver::validate_lba(start).is_err()
                    || virtio_driver::validate_lba(start + len).is_err()
                {
                    completion_backlog.push(Completion::for_failed_req(
                        req,
                        Errno::EINVAL,
                        const_cstr!("invalid len or start offset"),
                    ));
                    return true;
                }

                if self.read_only {
                    fail_req_due_to_read_only(completion_backlog, req);
                    return true;
                }

                self.vq.discard(start, len, context)
            }
            RequestTypeArgs::Flush => {
                if !self.features.contains(VirtioBlkFeatureFlags::FLUSH) {
                    completion_backlog.push(Completion::for_successful_req(req));
                    return true;
                }

                self.vq.flush(context)
            }
        };

        result.is_ok()
    }

    fn do_io(
        &mut self,
        request_backlog: &mut RequestBacklog,
        completion_backlog: &mut CompletionBacklog,
        completions: &mut [std::mem::MaybeUninit<Completion>],
        min_completions: usize,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<usize> {
        if min_completions != 0 || timeout.is_some() || sig.is_some() {
            return Err(Error::new(Errno::ENOTSUP, "Not implemented"));
        }

        // filled_completions tracks how many elements of completions[] have been filled in
        let mut filled_completions = completion_backlog.fill_completions(completions);

        let mut should_process_request_backlog = false;

        // We call `VirtioBlkQueue::completions` on each iteration instead of reusing the iterator
        // so that we can borrow `self` mutably for the `enqueue_or_backlog()` call below.
        // `VirtioBlkQueue::completions` is cheap, so this shouldn't be too problematic.
        while filled_completions < completions.len() {
            let completion = match self.vq.completions().next() {
                Some(c) => c,
                None => break,
            };

            should_process_request_backlog = true;

            if !completion.context.needs_flush || completion.ret != 0 {
                unsafe {
                    completions[filled_completions]
                        .as_mut_ptr()
                        .write(Completion {
                            user_data: completion.context.user_data,
                            ret: completion.ret,
                            error_msg: ptr::null(),
                        })
                };
                filled_completions += 1;
            } else {
                let req = Request {
                    args: RequestTypeArgs::Flush,
                    user_data: completion.context.user_data,
                    flags: ReqFlags::empty(),
                };
                request_backlog.enqueue_or_backlog(self, completion_backlog, req);
            }
        }

        if should_process_request_backlog {
            request_backlog.process(self, completion_backlog);
        }

        // TODO optimization: reduce latency by submitting at the beginning of the function when
        // the request backlog is empty.
        match self
            .submission_fd
            .write(1)
            .map_err(|e| Error::from_io_error(e, Errno::EIO))
        {
            Err(err) => {
                completion_backlog.unfill_completions(completions, filled_completions);
                Err(err)
            }
            Ok(_) => Ok(filled_completions),
        }
    }
}

properties! {
    VIRTIO_BLK_PROPS: PropertyState for VirtioBlk.props {
        buf_alignment: i32,
        fn capacity: u64,
        discard_alignment: i32,
        discard_alignment_offset: i32,
        driver: str,
        //mut fd: i32,
        max_discard_len: u64,
        fn max_queues: i32,
        max_queue_size: i32,
        fn max_mem_regions: u64,
        max_segments: i32,
        max_transfer: i32,
        max_write_zeroes_len: u64,
        mem_region_alignment: u64,
        needs_mem_regions: bool,
        needs_mem_region_fd: bool,
        mut num_queues: i32,
        optimal_buf_alignment: i32,
        optimal_io_alignment: i32,
        optimal_io_size: i32,
        mut path: str,
        mut queue_size: i32,
        mut read_only: bool,
        request_alignment: i32,
        supports_fua_natively: bool
    }
}

pub struct VirtioBlk {
    state: State,
    props: PropertyState,
    queues: Vec<Blkioq>,
    features: Option<VirtioBlkFeatureFlags>,
    transport: Option<RefCell<Box<VirtioBlkTransport>>>,
}

impl VirtioBlk {
    pub fn new(driver: &str) -> Self {
        VirtioBlk {
            props: PropertyState {
                buf_alignment: 1,
                discard_alignment: 512,
                discard_alignment_offset: 0,
                driver: driver.to_string(),
                //fd: -1,
                max_discard_len: 0,
                max_queue_size: MAX_QUEUE_SIZE,
                max_segments: MAX_QUEUE_SIZE as i32,
                max_transfer: 0,
                max_write_zeroes_len: 0,
                mem_region_alignment: 1,
                needs_mem_regions: true,
                needs_mem_region_fd: true,
                num_queues: 1,
                optimal_buf_alignment: 1,
                optimal_io_alignment: 512,
                optimal_io_size: 0,
                queue_size: DEFAULT_QUEUE_SIZE,
                path: String::new(),
                read_only: false,
                request_alignment: 512,
                supports_fua_natively: false,
            },
            queues: Vec::new(),
            state: State::Created,
            features: None,
            transport: None,
        }
    }

    // FIXME Share this code with io_uring
    fn cant_set_while_connected(&self) -> Result<()> {
        if self.state >= State::Connected {
            Err(properties::error_cant_set_while_connected())
        } else {
            Ok(())
        }
    }

    fn cant_set_while_started(&self) -> Result<()> {
        if self.state >= State::Started {
            Err(properties::error_cant_set_while_started())
        } else {
            Ok(())
        }
    }

    fn must_be_connected(&self) -> Result<()> {
        if self.state >= State::Connected {
            Ok(())
        } else {
            Err(properties::error_must_be_connected())
        }
    }

    fn must_be_started(&self) -> Result<()> {
        if self.state >= State::Started {
            Ok(())
        } else {
            Err(Error::new(Errno::EBUSY, "Device must be started"))
        }
    }

    fn get_transport(&self) -> RefMut<'_, Box<VirtioBlkTransport>> {
        self.transport.as_ref().unwrap().borrow_mut()
    }

    fn get_capacity(&self) -> Result<u64> {
        self.must_be_connected()?;

        let cfg = self
            .get_transport()
            .get_config()
            .map_err(|e| Error::from_io_error(e, Errno::EIO))?;
        Ok(512 * u64::from(cfg.capacity))
    }

    fn get_max_queues(&self) -> Result<i32> {
        self.must_be_connected()?;

        Ok(self.get_transport().max_queues() as i32)
    }

    fn get_max_mem_regions(&self) -> Result<u64> {
        self.must_be_connected()?;

        Ok(self.get_transport().max_mem_regions())
    }

    fn set_queue_size(&mut self, value: i32) -> Result<()> {
        self.must_be_connected()?;
        self.cant_set_while_started()?;

        if value <= 0 {
            return Err(Error::new(
                Errno::EINVAL,
                "queue_size must be greater than 0",
            ));
        }
        if !(value as u32).is_power_of_two() {
            return Err(Error::new(
                Errno::EINVAL,
                "queue_size must be a power of two",
            ));
        }
        if value > MAX_QUEUE_SIZE {
            return Err(Error::new(
                Errno::EINVAL,
                format!("queue_size must be smaller than {}", MAX_QUEUE_SIZE),
            ));
        }

        self.props.queue_size = value;
        Ok(())
    }

    fn set_num_queues(&mut self, value: i32) -> Result<()> {
        self.must_be_connected()?;
        self.cant_set_while_started()?;

        if value <= 0 {
            return Err(Error::new(
                Errno::EINVAL,
                "num_queues must be greater than 0",
            ));
        }

        let max_queues = self.get_max_queues()?;
        if value > max_queues {
            return Err(Error::new(
                Errno::EINVAL,
                format!("num_queues must not be greater than {}", max_queues),
            ));
        }

        self.props.num_queues = value;
        Ok(())
    }

    fn set_path(&mut self, value: &str) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.path = value.to_string();
        Ok(())
    }

    fn set_read_only(&mut self, value: bool) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.read_only = value;
        Ok(())
    }
}

impl Driver for VirtioBlk {
    fn state(&self) -> State {
        self.state
    }

    fn connect(&mut self) -> Result<()> {
        self.cant_set_while_started()?;

        if self.state == State::Connected {
            return Ok(());
        }

        if self.props.path.is_empty() {
            return Err(Error::new(Errno::EINVAL, "path must be set"));
        }

        let blk_features = VirtioBlkFeatureFlags::RO
            | VirtioBlkFeatureFlags::BLK_SIZE
            | VirtioBlkFeatureFlags::FLUSH
            | VirtioBlkFeatureFlags::TOPOLOGY
            | VirtioBlkFeatureFlags::MQ
            | VirtioBlkFeatureFlags::DISCARD
            | VirtioBlkFeatureFlags::WRITE_ZEROES;

        let features = blk_features.bits() | VirtioFeatureFlags::VERSION_1.bits();
        let mut vhost: Box<VirtioBlkTransport> = match self.props.driver.as_str() {
            #[cfg(feature = "driver-vhost-user")]
            VHOST_USER_DRIVER => Box::new(
                virtio_driver::VhostUser::new(&self.props.path, features)
                    .map_err(|_e| Error::new(Errno::EIO, "Failed to connect to vhost socket"))?,
            ),
            #[cfg(feature = "driver-vhost-vdpa")]
            VHOST_VDPA_DRIVER => Box::new(
                virtio_driver::VhostVdpa::new(&self.props.path, features)
                    .map_err(|_e| Error::new(Errno::EIO, "Failed to connect to vDPA device"))?,
            ),
            _ => return Err(Error::new(Errno::ENOENT, "Unknown driver name")),
        };

        let features = VirtioBlkFeatureFlags::from_bits_truncate(vhost.get_features());
        let cfg = vhost
            .get_config()
            .map_err(|e| Error::from_io_error(e, Errno::EIO))?;

        if features.contains(VirtioBlkFeatureFlags::DISCARD) {
            self.props.discard_alignment = 512 * cfg.discard_sector_alignment.to_native() as i32;
            self.props.max_discard_len = 512 * cfg.max_discard_sectors.to_native() as u64;
        }

        if features.contains(VirtioBlkFeatureFlags::WRITE_ZEROES) {
            self.props.max_write_zeroes_len = 512 * cfg.max_write_zeroes_sectors.to_native() as u64;
        }

        let blk_size = if features.contains(VirtioBlkFeatureFlags::BLK_SIZE) {
            cfg.blk_size.to_native() as i32
        } else {
            512
        };

        self.props.request_alignment = blk_size;
        self.props.optimal_io_alignment = blk_size;

        if features.contains(VirtioBlkFeatureFlags::TOPOLOGY) {
            self.props.optimal_io_alignment = blk_size * 2i32.pow(cfg.physical_block_exp as u32);
            self.props.optimal_io_size = blk_size * cfg.opt_io_size.to_native() as i32;
            self.props.discard_alignment_offset = blk_size * cfg.alignment_offset as i32;
        }

        self.features = Some(features);
        self.transport = Some(RefCell::new(vhost));
        self.state = State::Connected;

        Ok(())
    }

    fn start(&mut self) -> Result<()> {
        self.must_be_connected()?;

        if self.state == State::Started {
            return Ok(());
        }

        let mut transport = self.transport.as_ref().unwrap().borrow_mut();
        let features = VirtioBlkFeatureFlags::from_bits_truncate(transport.get_features());

        if features.contains(VirtioBlkFeatureFlags::RO) && !self.props.read_only {
            return Err(Error::new(Errno::EROFS, "Device is read-only"));
        }

        let queues = VirtioBlkQueue::setup_queues(
            transport.as_mut(),
            self.props.num_queues as usize,
            self.props.queue_size as u16,
        );

        fn set_nonblock(fd: RawFd) -> Result<()> {
            let status_flags = fcntl(fd, FcntlArg::F_GETFL)?;
            let status_flags = unsafe { OFlag::from_bits_unchecked(status_flags) };

            fcntl(fd, FcntlArg::F_SETFL(status_flags | OFlag::O_NONBLOCK))?;

            Ok(())
        }

        self.queues = queues
            .map_err(|e| Error::from_io_error(e, Errno::EIO))?
            .into_iter()
            .enumerate()
            .map(|(i, q)| {
                let submission_fd = transport.get_submission_fd(i);
                let completion_fd = transport.get_completion_fd(i);

                set_nonblock(completion_fd.as_raw_fd())?;

                Ok(Blkioq::new(Box::new(Queue::new(
                    self.features.unwrap(),
                    q,
                    Rc::clone(&submission_fd),
                    Rc::clone(&completion_fd),
                    self.props.read_only,
                ))))
            })
            .collect::<Result<_>>()?;

        self.state = State::Started;
        Ok(())
    }

    fn add_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        self.must_be_started()?;
        self.get_transport()
            .add_mem_region(region.addr, region.len, region.fd, region.fd_offset)
            .map_err(|e| Error::from_io_error(e, Errno::EIO))
    }

    fn del_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        self.must_be_started()?;
        self.get_transport()
            .del_mem_region(region.addr, region.len)
            .map_err(|e| Error::from_io_error(e, Errno::EIO))
    }

    fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq> {
        self.queues
            .get_mut(index)
            .ok_or_else(|| Error::new(Errno::EINVAL, "invalid queue index"))
    }
}
