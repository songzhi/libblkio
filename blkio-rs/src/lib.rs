// SPDX-License-Identifier: LGPL-2.1-or-later

#![cfg_attr(feature = "_unsafe-op-in-unsafe-fn", deny(unsafe_op_in_unsafe_fn))]
#![cfg_attr(not(feature = "_unsafe-op-in-unsafe-fn"), allow(unused_unsafe))]

mod drivers;
mod properties;

use crate::properties::Properties;
use bitflags::bitflags;
use const_cstr::{const_cstr, ConstCStr};
use libc::{c_char, c_void, iovec, off_t, sigset_t};
use nix::errno::Errno;
use nix::sys::memfd::{memfd_create, MemFdCreateFlag};
use nix::sys::mman::{mmap, munmap, MapFlags, ProtFlags};
use nix::unistd::{close, ftruncate};
use std::borrow::Cow;
use std::collections::{HashMap, VecDeque};
use std::ffi::CStr;
use std::fmt;
use std::fs::File;
use std::io;
use std::os::unix::io::RawFd;
use std::os::unix::io::{FromRawFd, IntoRawFd};
use std::result;
use std::time::Duration;
use std::{error, ptr};

// Must be kept in sync with include/blkio.h. Also, when adding a flag, make sure to add a
// corresponding arm in the match expression in validate_req_flags().
bitflags! {
    #[repr(transparent)]
    pub struct ReqFlags: u32 {
        const FUA = 1 << 0;
        const NO_UNMAP = 1 << 1;
        const NO_FALLBACK = 1 << 2;
    }
}

/// Returns a [`Completion`] if the request's flags are invalid.
fn validate_req_flags(req: &Request, allowed: ReqFlags) -> Option<Completion> {
    if allowed.contains(req.flags) {
        None
    } else if !ReqFlags::all().contains(req.flags) {
        Some(Completion::for_failed_req(
            req,
            Errno::EINVAL,
            const_cstr!("unsupported bits in request flags"),
        ))
    } else {
        let first_disallowed_flag = 1 << (req.flags & !allowed).bits().trailing_zeros();
        let first_disallowed_flag = ReqFlags::from_bits(first_disallowed_flag).unwrap();

        let error_msg = match first_disallowed_flag {
            ReqFlags::FUA => const_cstr!("BLKIO_REQ_FUA is invalid for this request type"),
            ReqFlags::NO_UNMAP => {
                const_cstr!("BLKIO_REQ_NO_UNMAP is invalid for this request type")
            }
            ReqFlags::NO_FALLBACK => {
                const_cstr!("BLKIO_REQ_NO_FALLBACK is invalid for this request type")
            }
            _ => panic!(),
        };

        Some(Completion::for_failed_req(req, Errno::EINVAL, error_msg))
    }
}

#[derive(Debug)]
pub struct Error {
    // We can't make `errno` a `nix::errno::Errno` because the latter is an enum and might not list
    // every possible errno value we can get.
    errno: i32,
    message: Cow<'static, str>,
}

impl Error {
    pub fn new<M>(errno: Errno, message: M) -> Self
    where
        Cow<'static, str>: From<M>,
    {
        Self {
            errno: errno as i32,
            message: message.into(),
        }
    }

    pub fn from_io_error(io_error: io::Error, default_errno: Errno) -> Self {
        Self {
            errno: io_error.raw_os_error().unwrap_or(default_errno as i32),
            message: io_error.to_string().into(),
        }
    }

    pub fn from_last_os_error() -> Self {
        let io_error = io::Error::last_os_error();
        Self {
            errno: io_error.raw_os_error().unwrap(),
            message: io_error.to_string().into(),
        }
    }

    pub fn errno(&self) -> i32 {
        self.errno
    }

    pub fn message(&self) -> &str {
        &self.message
    }
}

impl error::Error for Error {}

impl From<Errno> for Error {
    fn from(nix_error: Errno) -> Self {
        Self {
            errno: nix_error as i32,
            message: nix_error.desc().into(),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

pub type Result<T> = result::Result<T, Error>;

// This is the same as struct blkio_completion
#[repr(C)]
pub struct Completion {
    pub user_data: usize,
    pub ret: i32,
    pub error_msg: *const c_char,
}

impl Completion {
    pub(crate) fn for_successful_req(req: &Request) -> Self {
        Self {
            user_data: req.user_data,
            ret: 0,
            error_msg: ptr::null(),
        }
    }

    /// Build a Completion for a given Request
    ///
    /// Use [`const_cstr::const_cstr!`] to construct an error message. [`const_cstr::ConstCStr`] is
    /// used instead of `&'static str` so that callers don't have to remember to add a trailing
    /// `\0`.
    pub(crate) fn for_failed_req(req: &Request, errno: Errno, error_msg: ConstCStr) -> Self {
        Self {
            user_data: req.user_data,
            ret: -(errno as i32),
            error_msg: error_msg.as_ptr(),
        }
    }
}

pub(crate) enum RequestTypeArgs {
    Read {
        start: u64,
        buf: *mut u8,
        len: usize,
    },
    Write {
        start: u64,
        buf: *const u8,
        len: usize,
    },
    Readv {
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
    },
    Writev {
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
    },
    WriteZeroes {
        start: u64,
        len: u64,
    },
    Discard {
        start: u64,
        len: u64,
    },
    Flush,
}

/// A handy struct for request arguments
pub(crate) struct Request {
    pub(crate) args: RequestTypeArgs,
    pub(crate) user_data: usize,
    pub(crate) flags: ReqFlags,
}

pub(crate) trait Queue {
    fn get_completion_fd(&self) -> RawFd;

    fn set_completion_fd_enabled(&mut self, enabled: bool);

    /// Enqueue a request if there is enough space, returning true on success.
    #[must_use]
    fn try_enqueue(&mut self, completion_backlog: &mut CompletionBacklog, req: &Request) -> bool;

    fn do_io(
        &mut self,
        request_backlog: &mut RequestBacklog,
        completion_backlog: &mut CompletionBacklog,
        completions: &mut [std::mem::MaybeUninit<Completion>],
        min_completions: usize,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<usize>;
}

/// Requests waiting to be enqueued or submitted. Used when `Queue::try_enqueue()` does not have
/// space for a request.
pub(crate) struct RequestBacklog {
    reqs: VecDeque<Request>,
}

impl RequestBacklog {
    fn new() -> RequestBacklog {
        RequestBacklog {
            reqs: VecDeque::new(),
        }
    }

    pub(crate) fn len(&self) -> usize {
        self.reqs.len()
    }

    /// Try to enqueue a request with `try_enqueue()`. If the queue is full, put the request on the
    /// backlog.
    fn enqueue_or_backlog(
        &mut self,
        queue: &mut dyn Queue,
        completion_backlog: &mut CompletionBacklog,
        req: Request,
    ) {
        if !(self.reqs.is_empty() && queue.try_enqueue(completion_backlog, &req)) {
            self.reqs.push_back(req);
        }
    }

    /// Enqueue as many backlogged requests as possible and return the count
    pub(crate) fn process(
        &mut self,
        queue: &mut dyn Queue,
        completion_backlog: &mut CompletionBacklog,
    ) -> usize {
        let mut count = 0;
        while let Some(req) = self.reqs.pop_front() {
            if !queue.try_enqueue(completion_backlog, &req) {
                self.reqs.push_front(req); // reinsert the request into the backlog
                break;
            }
            count += 1;
        }
        count
    }
}

/// Completions waiting to be reaped by the application. Typically used for ENOTSUP and EINVAL
/// cases while enqueuing requests. Also used to hold completions until the next call when
/// Queue.do_io() has reaped some completions but needs to return an error.
pub(crate) struct CompletionBacklog {
    completions: VecDeque<Completion>,
    completion_fd: RawFd,
}

impl CompletionBacklog {
    fn new(completion_fd: RawFd) -> Self {
        Self {
            completions: VecDeque::new(),
            completion_fd,
        }
    }

    /// Notify the application that completions are available
    fn signal_completion_fd(&mut self) {
        let val: u64 = 1;
        let valp: *const u64 = &val;
        unsafe { libc::write(self.completion_fd, valp.cast(), std::mem::size_of::<u64>()) };
    }

    /// Add a Completion to the backlog
    pub(crate) fn push(&mut self, completion: Completion) {
        self.completions.push_back(completion);
        self.signal_completion_fd();
    }

    /// Fill completions[] from the backlog, oldest-first
    pub(crate) fn fill_completions(
        &mut self,
        completions: &mut [std::mem::MaybeUninit<Completion>],
    ) -> usize {
        let mut n = 0;
        for c in completions.iter_mut().take(self.completions.len()) {
            let val = self.completions.pop_front().unwrap();
            unsafe { c.as_mut_ptr().write(val) };
            n += 1;
        }
        n
    }

    /// Prepend filled completions[] elements to the backlog
    pub(crate) fn unfill_completions(
        &mut self,
        completions: &mut [std::mem::MaybeUninit<Completion>],
        count: usize,
    ) {
        for c in completions[..count].iter().rev() {
            self.completions.push_front(unsafe { c.as_ptr().read() });
        }
        self.signal_completion_fd();
    }
}

pub struct Blkioq {
    queue: Box<dyn Queue>,
    request_backlog: RequestBacklog,
    completion_backlog: CompletionBacklog,
}

impl Blkioq {
    pub(crate) fn new(queue: Box<dyn Queue>) -> Self {
        let completion_fd = queue.get_completion_fd();
        Blkioq {
            queue,
            request_backlog: RequestBacklog::new(),
            completion_backlog: CompletionBacklog::new(completion_fd),
        }
    }

    pub fn get_completion_fd(&self) -> RawFd {
        self.queue.get_completion_fd()
    }

    pub fn set_completion_fd_enabled(&mut self, enabled: bool) {
        self.queue.set_completion_fd_enabled(enabled);
    }

    fn enqueue(&mut self, allowed_flags: ReqFlags, req: Request) {
        if let Some(completion) = validate_req_flags(&req, allowed_flags) {
            self.completion_backlog.push(completion);
            return;
        }

        self.request_backlog
            .enqueue_or_backlog(&mut *self.queue, &mut self.completion_backlog, req)
    }

    pub fn read(
        &mut self,
        start: u64,
        buf: *mut u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) {
        self.enqueue(
            ReqFlags::empty(),
            Request {
                args: RequestTypeArgs::Read { start, buf, len },
                user_data,
                flags,
            },
        )
    }

    pub fn write(
        &mut self,
        start: u64,
        buf: *const u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) {
        self.enqueue(
            ReqFlags::FUA,
            Request {
                args: RequestTypeArgs::Write { start, buf, len },
                user_data,
                flags,
            },
        )
    }

    pub fn readv(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: ReqFlags,
    ) {
        let req = Request {
            args: RequestTypeArgs::Readv {
                start,
                iovec,
                iovcnt,
            },
            user_data,
            flags,
        };

        if iovcnt > i32::MAX as u32 {
            self.completion_backlog.push(Completion::for_failed_req(
                &req,
                Errno::EINVAL,
                const_cstr!("iovcnt must be non-negative and fit in a signed 32-bit integer"),
            ));
            return;
        }

        self.enqueue(ReqFlags::empty(), req)
    }

    pub fn writev(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: ReqFlags,
    ) {
        let req = Request {
            args: RequestTypeArgs::Writev {
                start,
                iovec,
                iovcnt,
            },
            user_data,
            flags,
        };

        if iovcnt > i32::MAX as u32 {
            self.completion_backlog.push(Completion::for_failed_req(
                &req,
                Errno::EINVAL,
                const_cstr!("iovcnt must be non-negative and fit in a signed 32-bit integer"),
            ));
            return;
        }

        self.enqueue(ReqFlags::FUA, req)
    }

    pub fn write_zeroes(&mut self, start: u64, len: u64, user_data: usize, flags: ReqFlags) {
        self.enqueue(
            ReqFlags::NO_UNMAP | ReqFlags::NO_FALLBACK,
            Request {
                args: RequestTypeArgs::WriteZeroes { start, len },
                user_data,
                flags,
            },
        )
    }

    pub fn discard(&mut self, start: u64, len: u64, user_data: usize, flags: ReqFlags) {
        self.enqueue(
            ReqFlags::empty(),
            Request {
                args: RequestTypeArgs::Discard { start, len },
                user_data,
                flags,
            },
        )
    }

    pub fn flush(&mut self, user_data: usize, flags: ReqFlags) {
        self.enqueue(
            ReqFlags::empty(),
            Request {
                args: RequestTypeArgs::Flush,
                user_data,
                flags,
            },
        );
    }

    pub fn do_io(
        &mut self,
        completions: &mut [std::mem::MaybeUninit<Completion>],
        min_completions: usize,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<usize> {
        self.queue.do_io(
            &mut self.request_backlog,
            &mut self.completion_backlog,
            completions,
            min_completions,
            timeout,
            sig,
        )
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd)]
enum State {
    Created,   // after blkio_new()
    Connected, // after blkio_connect()
    Started,   // after blkio_start()
}

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct MemoryRegion {
    pub addr: usize,
    pub iova: u64,
    pub len: usize,
    pub fd: RawFd,
    pub fd_offset: i64,
    pub flags: u32,
}

trait Driver: Properties {
    fn state(&self) -> State;
    fn connect(&mut self) -> Result<()>;
    fn start(&mut self) -> Result<()>;

    /// The allocated region is _not_ added (registered) by this method.
    fn alloc_mem_region(&mut self, len: usize) -> Result<MemoryRegion> {
        if self.state() < State::Connected {
            return Err(properties::error_must_be_connected());
        }

        let align = self.get_u64("mem-region-alignment")? as usize;

        if len % align != 0 {
            return Err(Error::new(
                Errno::EINVAL,
                format!("len {} violates mem-region-alignment {}", len, align),
            ));
        }

        let fd = memfd_create(
            CStr::from_bytes_with_nul(b"libblkio-buf\0").unwrap(),
            MemFdCreateFlag::MFD_CLOEXEC,
        )?;

        // Automatically close the fd in error code paths
        let file = unsafe { File::from_raw_fd(fd) };

        ftruncate(fd, len as off_t)?;

        let addr = unsafe {
            mmap(
                ptr::null_mut(),
                len,
                ProtFlags::PROT_READ | ProtFlags::PROT_WRITE,
                MapFlags::MAP_SHARED,
                fd,
                0,
            )?
        };

        // Give up if the address is unaligned. Don't attempt to align manually because
        // "mem-region-alignment" should not exceed the page size in practice.
        if (addr as usize) % align != 0 {
            unsafe { munmap(addr, len)? };

            return Err(Error::new(
                Errno::EOVERFLOW,
                format!(
                    "Address {} violates mem-region-alignment {}",
                    addr as usize, align,
                ),
            ));
        }

        Ok(MemoryRegion {
            addr: addr as usize,
            iova: 0,
            len,
            fd: file.into_raw_fd(),
            fd_offset: 0,
            flags: 0,
        })
    }

    /// The given region must _not_ be registered when this method is called.
    fn free_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        if self.state() < State::Connected {
            return Err(properties::error_must_be_connected());
        }

        unsafe { munmap(region.addr as *mut c_void, region.len)? };
        close(region.fd)?;

        Ok(())
    }

    fn add_mem_region(&mut self, region: &MemoryRegion) -> Result<()>;
    fn del_mem_region(&mut self, region: &MemoryRegion) -> Result<()>;

    fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq>;
}

pub struct Blkio {
    driver: Box<dyn Driver>,
    allocated_regions: HashMap<MemoryRegion, bool>, // value is true iff region is registered
}

impl Blkio {
    pub fn new(driver_name: &str) -> Result<Blkio> {
        let driver: Box<dyn Driver> = match driver_name {
            #[cfg(feature = "driver-io-uring")]
            "io_uring" => Box::new(drivers::iouring::IoUring::new()),
            #[cfg(feature = "driver-vhost-user")]
            drivers::virtio_blk::VHOST_USER_DRIVER => {
                Box::new(drivers::virtio_blk::VirtioBlk::new(driver_name))
            }
            #[cfg(feature = "driver-vhost-vdpa")]
            drivers::virtio_blk::VHOST_VDPA_DRIVER => {
                Box::new(drivers::virtio_blk::VirtioBlk::new(driver_name))
            }
            _ => return Err(Error::new(Errno::ENOENT, "Unknown driver name")),
        };

        Ok(Blkio {
            driver,
            allocated_regions: HashMap::new(),
        })
    }

    pub fn connect(&mut self) -> Result<()> {
        self.driver.connect()
    }

    pub fn start(&mut self) -> Result<()> {
        self.driver.start()
    }

    pub fn get_bool(&self, name: &str) -> Result<bool> {
        self.driver.get_bool(name)
    }

    pub fn get_i32(&self, name: &str) -> Result<i32> {
        self.driver.get_i32(name)
    }

    pub fn get_str(&self, name: &str) -> Result<String> {
        self.driver.get_str(name)
    }

    pub fn get_u64(&self, name: &str) -> Result<u64> {
        self.driver.get_u64(name)
    }

    pub fn set_bool(&mut self, name: &str, value: bool) -> Result<()> {
        self.driver.set_bool(name, value)
    }

    pub fn set_i32(&mut self, name: &str, value: i32) -> Result<()> {
        self.driver.set_i32(name, value)
    }

    pub fn set_str(&mut self, name: &str, value: &str) -> Result<()> {
        self.driver.set_str(name, value)
    }

    pub fn set_u64(&mut self, name: &str, value: u64) -> Result<()> {
        self.driver.set_u64(name, value)
    }

    /// The allocated region is _not_ added (registered) by this method.
    pub fn alloc_mem_region(&mut self, len: usize) -> Result<MemoryRegion> {
        let region = self.driver.alloc_mem_region(len)?;
        self.allocated_regions.insert(region, false);
        Ok(region)
    }

    /// The given region must _not_ be registered when this method is called.
    pub fn free_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        self.allocated_regions.remove(region);
        self.driver.free_mem_region(region)
    }

    pub fn add_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        let align = self.get_u64("mem-region-alignment")? as usize;

        if region.addr % align != 0 {
            return Err(Error::new(
                Errno::EINVAL,
                format!(
                    "addr {:#x} violates mem-region-alignment {}",
                    region.addr, align
                ),
            ));
        }

        if region.len % align != 0 {
            return Err(Error::new(
                Errno::EINVAL,
                format!(
                    "len {:#x} violates mem-region-alignment {}",
                    region.len, align
                ),
            ));
        }

        self.driver.add_mem_region(region)?;

        self.allocated_regions
            .entry(*region)
            .and_modify(|s| *s = true);

        Ok(())
    }

    pub fn del_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        self.allocated_regions
            .entry(*region)
            .and_modify(|s| *s = false);

        self.driver.del_mem_region(region)
    }

    pub fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq> {
        self.driver.get_queue(index)
    }
}

impl Drop for Blkio {
    fn drop(&mut self) {
        for (region, &is_registered) in &self.allocated_regions {
            if is_registered {
                let _ = self.driver.del_mem_region(region);
            }

            let _ = self.driver.free_mem_region(region);
        }
    }
}
