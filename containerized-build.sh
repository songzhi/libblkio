#!/bin/sh
cd "$(dirname $0)"

# Build image, if necessary
if [ $(podman images --noheading --filter reference=libblkio-builder | wc -l) -eq 0 ]; then
    buildah bud --tag libblkio-builder .
fi

# Disable seccomp so io_uring syscalls are allowed
exec podman run --security-opt=seccomp=unconfined --rm -it --user "$(id --user):$(id --group)" --userns keep-id --volume .:/usr/src/libblkio:z libblkio-builder ./build.sh
