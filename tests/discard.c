// SPDX-License-Identifier: LGPL-2.1-or-later
#include <sys/mman.h>
#include "util.h"

enum {
    TEST_FILE_SIZE = 2 * 1024 * 1024,
    TEST_FILE_OFFSET = 0x10000,
};

static char filename[] = "discard-XXXXXX";

static void cleanup(void)
{
    unlink(filename);
}

/*
 * Verify that blkioq_discard() succeeds.
 */
int main(int argc, char **argv)
{
    struct test_opts opts;
    struct blkio *b;
    struct blkioq *q;
    struct blkio_completion completion;
    size_t page_size;
    int fd;

    parse_generic_opts(&opts, argc, argv);

    page_size = sysconf(_SC_PAGESIZE);

    ok(blkio_create(opts.driver, &b));
    assert(b);

    register_cleanup(cleanup);
    fd = create_file(filename, TEST_FILE_SIZE);

    ok(blkio_set_int(b, "fd", fd));
    ok(blkio_connect(b));

    fd = -1; /* ownership passed to libblkio */

    ok(blkio_set_int(b, "num-queues", 1));
    ok(blkio_start(b));

    q = blkio_get_queue(b, 0);
    assert(q);

    blkioq_discard(q, TEST_FILE_OFFSET, page_size, NULL, 0);

    assert(blkioq_do_io(q, &completion, 1, 1, NULL) == 1);
    skip_if(completion.ret == -ENOTSUP);
    assert(completion.ret == 0);

    blkio_destroy(&b);
    return 0;
}
